.model small
.code
org 100h
start:
	jmp mulai
nama	    db 13,10,'Nama Anda	     : $'
lanjut	    db 13,10,'LANJUT Tekan y untuk lanjut >>>>>>>>>>>>> $'
tampung_nama	db 30,?,30 dup(?)
tampung_hp	db 13,?,13 dup(?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar1 db 13,10,'		UANG BULANAN 1.000.000			'
	db 13,10,'+-----------------------------------------------------+'
	db 13,10,'|		PENGELUARAN BIAYA BULANAN	       |'
	db 13,10,'|---+---------------------------------+---------------+'
	db 13,10,'|No | NAMA				| Harga	       |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|01 | LISTRIK 				| Rp. 400.000 |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|02 | KEPERLUAN MANDI			| Rp. 100.000 |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|03 | SERVIS MOTOR			| Rp. 200.000 |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|04 | KUOTA INTERNET			| Rp. 70.000  |'
	db 13,10,'+---+---------------------------------+---------------+'
	db 13,10,'|05 | AIR 				| Rp. 50.000  |'
	db 13,10,'+---+---------------------------------+---------------+','$'


pilih_msk   db 13,10,'Silahkan masukkan No yang anda pilih: $'
eror	    db 13,10,'NOMOR YG ANDA MASUKKAN SALAH $'
success	    db 13,10,'Selamat Anda Berhasil $'

	mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx

	mov ah,09h
	mov dx,offset daftar1
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01
	int 21h
	cmp al,'y'
	je proses
	jne error_msg

error_msg:
	mov ah,09h
	mov dx,offset error_msg
	int 21h
	int 20h

proses:
	mov ah,09h
	mov dx,offset pilih_msk
	int 21h
	mov ah,1
	int 21h
	mov bh,al
	mov ah,1
	int 21h
	mov bl,al
	
	cmp bh,'0'
	cmp bl,'1'
	je hasil1

	cmp bh,'0'
	cmp bl,'2'
	je hasil2

	cmp bh,'0'
	cmp bl,'3'
	je hasil3

	cmp bh,'0'
	cmp bl,'4'
	je hasil4

	cmp bh,'0'
	cmp bl,'5'
	je hasil5

	jne error_msg
;--------------------------------------------------------

hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h

hasil4:	
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

hasil5:
	mov ah,09h
	lea dx,teks5
	int 21h
	int 20h


;--------------------------------------------------------

teks1	db 13,10,'Total harga yang harus di bayar adalah RP. 400.000'
	db 13,10,'Untuk pembayaran listrik'
        db 13,10,'Sisa uang saku anda adalah Rp. 600.000' 
	db 13,10,'Terima Kasih $'

teks2	db 13,10,'Total harga yang harus di bayar adalah RP. 100.000'
	db 13,10,'Untuk keperluan mandi'
        db 13,10,'Sisa uang saku anda adalah Rp. 900.000' 
	db 13,10,'Terima Kasih $'

teks3	db 13,10,'Total harga yang harus di bayar adalah RP. 200.000'
	db 13,10,'Untuk servis motor bulanan'
        db 13,10,'Sisa uang saku anda adalah Rp. 800.000' 
	db 13,10,'Terima Kasih $'

teks4	db 13,10,'Total harga yang harus di bayar adalah RP. 70.000'
	db 13,10,'Untuk pembelian kuota internet'
        db 13,10,'Sisa uang saku anda adalah Rp. 93.000' 
	db 13,10,'Terima Kasih $'

teks5	db 13,10,'Total harga yang harus di bayar adalah RP. 50.000'
	db 13,10,'Untuk pembayaran air'
        db 13,10,'Sisa uang saku anda adalah Rp. 950.000' 
	db 13,10,'Terima Kasih $'



end start